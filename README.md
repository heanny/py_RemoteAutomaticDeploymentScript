# python_远程自动部署脚本

#### 项目介绍
通过paramiko库进行远程ssh操作并部署

#### 软件架构
软件架构说明
1.自动获取server.txt文件的账户密码等信息
2.通过python库paramiko进行ssh，并且sftp上传jar包
3.“#”自动忽略问题
4.上传进度条显示
5.修复Windows下kill远程java程序


#### 安装教程

1. 安装python
2. 安装依赖paramiko，tqdm
3. 修改server.txt文件，添加相关信息
4. 修改jar包（同理可上传部署其他文件：ngix等）

#### 版本更新说明

1. v1.* 自动化部署ngix
2. v2.1 使用实体类，根据server.txt文件进行部署，修复ssh后的环境问题
2. v2.2 新增上传进度功能
2. v2.3 修复Windows下的kill进程问题
2. v2.4 新增“#”注释忽略的问题，server中可增加注释进行忽略相关服务器
2. v2.5 修改上传进度动画，调用类进行更新显示
2. v2.6 上供Mumu祭天

#### 参与贡献

1. Heanny（作者）
2. Mumu（测试）



